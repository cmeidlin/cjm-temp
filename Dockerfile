FROM registry.access.redhat.com/rhoar-nodejs/nodejs-10

WORKDIR /opt/app-root/src
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 8080

CMD [ "npm", "start" ]
